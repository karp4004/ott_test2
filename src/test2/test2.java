package test2;


import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

public class test2 {
	
	static int n = 20;
	static int size = (2*n-1);
	
	{
		size = size*size;
	}
	
	public static void main(String[] args) {	
		
		Random rand = new Random();
		
		int[][] matrix = new int[size][size];
		for(int r=0;r<size;r++)
		{
			for(int c=0;c<size;c++)
			{
				matrix[r][c] = rand.nextInt(10);
				System.out.print(String.format("%d ", matrix[r][c]));
			}
			
			System.out.print("\n");
		}
		
		System.out.print("\n");
		
		int r=n-1;
		int c=n-1;
		int width = 0;
		int durection = 1;
		
		System.out.print(String.format("%d ", matrix[r][c]));
		
		while(r != 0)
		{
			width++;
			
			for(int j=0;j<width;j++)
			{
				c -= durection;
				System.out.print(String.format("%d ", matrix[r][c]));
			}
			
			for(int j=0;j<width;j++)
			{
				r += durection;
				System.out.print(String.format("%d ", matrix[r][c]));
			}
			
			durection = -durection;
		}
		
		for(int j=0;j<width;j++)
		{
			c--;
			System.out.print(String.format("%d ", matrix[r][c]));
		}
	}
}
